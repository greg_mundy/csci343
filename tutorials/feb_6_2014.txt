# Create the artist table
CREATE TABLE artist(id int NOT NULL, name varchar(20), date_of_birth date, PRIMARY KEY (id));

# Create the song table
CREATE TABLE song(id int NOT NULL, title varchar(20), length float(5,2), PRIMARY  KEY (id));

# Create the album table
CREATE TABLE album(id int NOT NULL, title varchar(20), year int, sales int, PRIMARY KEY (id));

# Create the genre table
CREATE TABLE genre(id int NOT NULL, name varchar(20), PRIMARY KEY (id));

# Add some artists
INSERT INTO artist(id, name, date_of_birth) VALUES(1, 'Bruno Mars', '1985-10-08');
INSERT INTO artist(id, name, date_of_birth) VALUES(2, 'Rihanna', '1988-02-20');
INSERT INTO artist(id, name, date_of_birth) VALUES(3, 'Brad Paisley', '1972-10-28');
INSERT INTO artist(id, name, date_of_birth) VALUES(4, 'Carrie Underwood', '1983-03-10');
INSERT INTO artist(id, name, date_of_birth) VALUES(5, 'Damian Marley', '1978-07-21');
INSERT INTO artist(id, name, date_of_birth) VALUES(6, 'Tarrus Riley', '1979-04-26');
INSERT INTO artist(id, name, date_of_birth) VALUES(7, 'Anthony Kiedis', '1962-11-01');
INSERT INTO artist(id, name, date_of_birth) VALUES(8, 'Flea', '1962-10-16');
INSERT INTO artist(id, name, date_of_birth) VALUES(9, 'Chad Smith', '1961-10-25');
INSERT INTO artist(id, name, date_of_birth) VALUES(10, 'Josh Klinghoffer', '1979-10-03');

# Add some songs
INSERT INTO song(id, title, length) VALUES(1,'Young Girls',3.39);
INSERT INTO song(id, title, length) VALUES(2,'Locked Out of Heaven',3.51);
INSERT INTO song(id, title, length) VALUES(3,'Gorilla',3.34);
INSERT INTO song(id, title, length) VALUES(4,'Treasure',2.56);
INSERT INTO song(id, title, length) VALUES(5,'Moonshine',3.42);
INSERT INTO song(id, title, length) VALUES(6,'When I Was Your Man',3.32);
INSERT INTO song(id, title, length) VALUES(7,'Natalie',3.45);
INSERT INTO song(id, title, length) VALUES(8,'Show Me',3.27);
INSERT INTO song(id, title, length) VALUES(9,'Money Make Her Smile',3.23);
INSERT INTO song(id, title, length) VALUES(10,'If I Knew',2.12);
INSERT INTO song(id, title, length) VALUES(11,'Phresh Out the Runway',3.42);
INSERT INTO song(id, title, length) VALUES(12,'Diamonds',3.34);
INSERT INTO song(id, title, length) VALUES(13,'Numb',3.25);
INSERT INTO song(id, title, length) VALUES(14,'Pour It Up',2.41);
INSERT INTO song(id, title, length) VALUES(15,'Loveeeeeee Song',4.16);
INSERT INTO song(id, title, length) VALUES(16,'Jump',4.24);
INSERT INTO song(id, title, length) VALUES(17,'Right Now',3.01);
INSERT INTO song(id, title, length) VALUES(18,'What Now',4.03);
INSERT INTO song(id, title, length) VALUES(19,'Stay',4.00);
INSERT INTO song(id, title, length) VALUES(20,'Nobody\'s Business',3.36);
INSERT INTO song(id, title, length) VALUES(21,'Love Without Tragedy / Mother Mary',6.58);
INSERT INTO song(id, title, length) VALUES(22,'Get It Over With',3.31);
INSERT INTO song(id, title, length) VALUES(23,'No Love Allowed',4.09);
INSERT INTO song(id, title, length) VALUES(24,'Lost in Paradise',3.35);
INSERT INTO song(id, title, length) VALUES(25,'Half of Me',3.12);
INSERT INTO song(id, title, length) VALUES(26,'Monarchy of Roses',4.11);
INSERT INTO song(id, title, length) VALUES(27,'Factory of Faith',4.19);
INSERT INTO song(id, title, length) VALUES(28,'Brendan\'s Death Song',5.38);
INSERT INTO song(id, title, length) VALUES(29,'Ethiopia',3.50);
INSERT INTO song(id, title, length) VALUES(30,'Annie Wants a Baby',3.40);
INSERT INTO song(id, title, length) VALUES(31,'Look Around',3.28);
INSERT INTO song(id, title, length) VALUES(32,'The Adventures of Rain Dance Maggie',4.42);
INSERT INTO song(id, title, length) VALUES(33,'Did I Let You Know',4.21);
INSERT INTO song(id, title, length) VALUES(34,'Goodbye Hooray',3.52);
INSERT INTO song(id, title, length) VALUES(35,'Happiness Loves Company',3.33);
INSERT INTO song(id, title, length) VALUES(36,'Police Station',5.35);
INSERT INTO song(id, title, length) VALUES(37,'Even You Brutus?',4.00);
INSERT INTO song(id, title, length) VALUES(38,'Meet Me at the Corner',4.21);
INSERT INTO song(id, title, length) VALUES(39,'Dance, Dance, Dance',3.45);

# Add some albums
INSERT INTO album(id,title,year,sales) VALUES(1,'I\'m With You', 2011, 150000); 
INSERT INTO album(id,title,year,sales) VALUES(2,'Unapologetic', 2012, 120000); 
INSERT INTO album(id,title,year,sales) VALUES(3,'Unorthodox Jukebox', 2012, 100000); 

# Add some genres
INSERT INTO genre(id, name) VALUES(1, 'Rock');
INSERT INTO genre(id, name) VALUES(2, 'Reggae');
INSERT INTO genre(id, name) VALUES(3, 'Pop');
INSERT INTO genre(id, name) VALUES(4, 'R&B');
INSERT INTO genre(id, name) VALUES(5, 'Country');
INSERT INTO genre(id, name) VALUES(6, 'Alternative');
INSERT INTO genre(id, name) VALUES(7, 'Hip Hop');

# Basic queries -- run each query and describe what it does...
SELECT * FROM artist;

SELECT * FROM song;

SELECT * FROM album;

SELECT * FROM genre;

SELECT name FROM genre;

SELECT title, year FROM album;

SELECT title AS Title, year as Year FROM album;

SELECT title AS 'ALBUM TITLE', year as 'YEAR RELEASED' FROM album;

SELECT COUNT(*) FROM song; 

SELECT COUNT(*) AS 'Total Songs' FROM song;

SELECT name FROM artist WHERE id = 5;

SELECT name FROM artist WHERE id IN (1, 3, 4);

SELECT id, name FROM artist where id IN (1, 3, 4);

SELECT name, date_of_birth FROM artist WHERE date_of_birth between '1977-01-01' AND '1985-12-31';

SELECT title FROM album WHERE title LIKE 'Un%';

SELECT id FROM genre WHERE name = 'Rock' OR name = 'Pop';

# The problem with many to many relationships...
# How do we know which album, artist, etc. are related?
# That's why we need join tables!

# artist_album
CREATE TABLE artist_album(artist_id int NOT NULL, album_id int NOT NULL, PRIMARY KEY(artist_id, album_id), FOREIGN KEY(artist_id) REFERENCES artist(id), FOREIGN KEY(album_id) REFERENCES album(id));

# artist_genre
CREATE TABLE artist_genre(artist_id int NOT NULL, genre_id int NOT NULL, PRIMARY KEY(artist_id, genre_id), FOREIGN KEY(artist_id) REFERENCES artist(id), FOREIGN KEY(genre_id) REFERENCES genre(id));

# song_genre
CREATE TABLE song_genre(song_id int NOT NULL, genre_id int NOT NULL, PRIMARY KEY(song_id, genre_id), FOREIGN KEY(song_id) REFERENCES song(id), FOREIGN KEY(genre_id) REFERENCES genre(id));

# artist_song
CREATE TABLE artist_song(artist_id int NOT NULL, song_id int NOT NULL, PRIMARY KEY(artist_id, song_id), FOREIGN KEY(artist_id) REFERENCES artist(id), FOREIGN KEY(song_id) REFERENCES song(id));

# artist_album
CREATE TABLE artist_album(artist_id int NOT NULL, album_id int NOT NULL, PRIMARY KEY(artist_id, album_id), FOREIGN KEY(artist_id) REFERENCES artist(id), FOREIGN KEY(album_id) REFERENCES album(id));

# album_song
CREATE TABLE album_song(album_id int NOT NULL, song_id int NOT NULL, PRIMARY KEY(album_id, song_id), FOREIGN KEY(album_id) REFERENCES album(id), FOREIGN KEY(song_id) REFERENCES song(id));

# album_genre
CREATE TABLE album_genre(album_id int NOT NULL, genre_id int NOT NULL, PRIMARY KEY(album_id, genre_id), FOREIGN KEY(album_id) REFERENCES album(id), FOREIGN KEY(genre_id) REFERENCES genre(id));

# more queries
SHOW TABLES;

DESCRIBE album;

DESCRIBE artist_album;

DESCRIBE artist_song;

DESCRIBE song_genre;

# Add some more data
INSERT INTO album_genre(album_id, genre_id) VALUES(1,1);
INSERT INTO album_genre(album_id, genre_id) VALUES(1,6);
INSERT INTO album_genre(album_id, genre_id) VALUES(2,3);
INSERT INTO album_genre(album_id, genre_id) VALUES(2,4);
INSERT INTO album_genre(album_id, genre_id) VALUES(3,1);

# A bit more complicated query
SELECT a.title, g.name FROM album a, album_genre ag, genre g where ag.album_id = a.id and ag.genre_id = g.id;


# A task for you, populate the other join tables and modify the above query
# for each.



